# DEVII Competencies

This repository gathers all my submissions for a python development course at
[Ephec's college](https://ephec.be).

## Project layout

The aforementioned course defines a series of competencies and associated
evaluations a student may complete to score points in the final evaluation.
Each competency's implementation is placed in a subdirectory of this repo's
root folder, with a name matching `dev[0-9]+`.

Each of these folder have the following layout:

```
devXXX
    README.md        # Module's explanation
    requirements.txt # Module's requirements
    setup.py         # Module's packaging script
    __init__.py
    __main__.py      # Optional
    # any other python source or other file needed
```

where `setup.py` is a modified copy of the `setup.py.template` file at the root
of this repository.

## Packaging

Each module provides a `setup.py` script. To run it, add the repository's root
directory to your `PYTHONPATH` and run `python setup.py` while in the module's
directory.

## Nix

This project is a [nix](https://nixos.org)
[flake](https://nixos.wiki/wiki/Flakes) and as such provides a
[devshell](https://github.com/numtide/devshell) and nix packages for each of
the competencies. Packages are named the same way their folders are.

To use any of the following features, you will need nix installed.

To enter the devshell, run `nix develop`. Entering the devshell is enough to
set up a complete development environment.

To run any of the apps of this repo without needing to clone it, run:

```
nix run gitlab:austreelis/dev2-competencies#dev202
```

To run tests, use `nix check`:

```
nix check gitlab:austreelis/dev2-competencies#dev202
```

You may even install them:

```
nix profile install gitlab:austreelis/dev2-competencies#dev202
```

See also the [nix manual](https://nixos.org/manual/nix/stable/).

## License

This repository is licensed under an MIT-like license forbidding use for some
organizations. For more information, please refer to `License.md`, or send an
email to `enquiries@license.austreelis.net`
