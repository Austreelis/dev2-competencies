from __future__ import annotations

import getopt
import io
import os
import sys
from datetime import datetime
from os import SEEK_CUR, SEEK_END, SEEK_SET
from typing import BinaryIO

import requests as requests

from dev202 import version

DESCRIPTION = """A python re-implementation of vim's xxd binary dumper"""

COPYRIGHT = """Copyright 2021 © Morgane Austreelis
Licensed under a custom MIT-like license
See https://gitlab.com/Austreelis/dev2-competencies/-/blob/main/LICENSE.md"""

USAGE = """Usage:
        dev-202 [options] [infile [outfile]]
Options:
    -c cols        format <cols> octets per line. Default 16.
    -e             little-endian dump.
    -g group_size  number of octets per group in normal output. Default 2.
    -h --help      print this summary.
    -l len         stop after <len> octets.
    -o off         add <off> to the displayed file position.
    -p dev_key     upload dump to pastebin and output url. Disables dumping to
                   stdout. To obtain your dev key, create a pastebin account
                   and go to the "API" tab.
    -d             show offset in decimal instead of hex.
    -s [+][-]seek  start at <seek> bytes abs. (or -: from end) infile offset.
    -u             use upper case hex letters.
    -v --version   show version."""


class ExitException(Exception):
    def __init__(self, status: int, msg: str | None = None):
        super(ExitException, self).__init__(msg)
        self.status = status

    def panic(self):
        """
        Print this error on stderr if given status is > 0 and exit the program.
        """
        if self.status != 0:
            print(str(self), file=sys.stderr)
        exit(self.status)


def parse_non_zero_int(arg: str, name: str) -> int:
    """
    Parses a non-zero positive integer option.

    Prints an error upon failure.

    :param arg: The option's value.
    :param name: The option's full name.
    :return: The integer value of the option, or -1 on error.
    """
    try:
        ret = int(arg)
    except ValueError:
        raise ExitException(
            2, f"Invalid {name} format '{arg}'\nExpected a base 10 integer"
        )
    # noinspection PyUnboundLocalVariable
    if ret < 1:
        raise ExitException(
            2, f"Invalid {name} format '{arg}'\nMust be greater than 0"
        )
    return ret


def parse_args() -> dict:
    """
    Parse `sys.argv`.

    :return: A dict of the program's arguments and options.
    """
    try:
        opts, files = getopt.getopt(
            sys.argv[1:],
            "hvc:deg:l:o:p:s:u",
            ["help", "version"]
        )
    except getopt.GetoptError:
        raise ExitException(2, USAGE)

    ret = {}

    # Parse options
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(DESCRIPTION, end="\n\n")
            print(COPYRIGHT)
            print(f"v{version}", end="\n\n")
            print(USAGE)
            raise ExitException(0)
        elif opt in ('-v', '--version'):
            print(version)
            raise ExitException(0)
        elif opt == '-c':
            ret["columns"] = parse_non_zero_int(arg, "column")
        elif opt == "-e":
            ret["little_endian"] = True
        elif opt == "-g":
            ret["group_size"] = parse_non_zero_int(arg, "group size")
        elif opt == "-l":
            ret["length"] = parse_non_zero_int(arg, "length")
        elif opt == "-o":
            ret["offset"] = parse_non_zero_int(arg, "offset")
        elif opt == "-p":
            ret["pastebin"] = arg
            # Suppressing standard output dump
            if "outfile" not in ret:
                ret["outfile"] = os.devnull
        elif opt == "-d":
            ret["decimal"] = True
        elif opt == "-s":
            try:
                ret["seek"] = int(arg)
            except ValueError:
                raise ExitException(
                    2,
                    f"Invalid seek format '{arg}'\nExpected a base 10 integer"
                )
        elif opt == "-u":
            ret["upper"] = True
        else:
            raise ExitException(2, USAGE)

    if not files:
        raise ExitException(2, USAGE)

    # First argument is mandatory
    ret["infile"] = files[0]

    if files[1:]:
        ret["outfile"] = files[1]

    if files[2:]:
        raise ExitException(2, USAGE)

    return ret


def fprint(f=sys.stdout, *args, **kwargs):
    """
    Like the print builtins, but the file argument is the first positional
    argument and the end argument defaults to an empty string.
    """
    print(*args, **{"file": f, "end": "", **kwargs})


def check_eof(f: BinaryIO) -> bool:
    """
    Check if we reached EOF on a file. Always returns `False` if the file is not
    seekable.

    :param f: The file to check.
    :return: `True` if the file is seekable and still has data.
    """
    if f.seekable() and f.read(1):
        f.seek(-1, SEEK_CUR)
        return True
    return False


def write_dump_line(
    infile: BinaryIO, outfile: io.TextIOWrapper, columns: int,
    group_size: int, offset: int, length: int, decimal: bool, upper: bool,
    little_endian: bool
):
    # Print offset
    if decimal:
        fprint(outfile, f"{offset:08d}")
    elif upper:
        fprint(outfile, f"{offset:08X}")
    else:
        fprint(outfile, f"{offset:08x}")
    fprint(outfile, ":")
    line = ""
    n_read_total = 0
    # Print each groups in each column
    for column in range(columns // group_size):
        group = infile.read(group_size)
        fprint(outfile, " ")
        if little_endian:
            group = reversed(group)
        n_read_group = 0
        for byte in group:
            # Check length limit
            if n_read_total + n_read_group >= length:
                break
            n_read_group += 1
            char = chr(byte)
            if char.isascii() and char.isprintable():
                line += char
            else:
                line += "."
            if upper:
                fprint(outfile, f"{byte:02X}")
            else:
                fprint(outfile, f"{byte:02x}")
        n_read_total += n_read_group
        for missing_octet in range(group_size - n_read_group):
            # EOF may have been reached at some point in the group
            # or right at the start, we still write blank spaces for
            # missing octets to properly align the text display on
            # the side
            fprint(outfile, "  ")
    fprint(outfile, f"  {line}\n")
    return n_read_total


def upload_paste(paste: str, name: str, api_key: str):
    """
    Upload some text to pastebin.

    :param paste: The paste's text.
    :param name: The paste's name.
    :param api_key: The dev api key used to upload the paste.
    :return: The uploaded paste's url.
    """
    request = requests.session().request(
        "POST", "https://pastebin.com/api/api_post.php", data={
            "api_user_key": "",
            "api_dev_key": api_key,
            "api_option": "paste",
            "api_paste_name": name,
            "api_paste_format": "",
            "api_paste_private": "1",
            "api_paste_expire_date": "1M",
            "api_paste_code": paste,
        },
    )
    response = request.content.decode()
    if request.status_code != 200:
        raise ExitException(1, response)
    return response


def main(
    # Program positional arguments
    infile: str | int,
    outfile: str | int = sys.stdout.fileno(),
    *,
    # Program options
    columns: int = 16,
    little_endian: bool = False,
    group_size: int = 2,
    length: int | None = None,
    offset: int = 0,
    pastebin: str | None = None,
    seek: int = 0,
    decimal: bool = False,
    upper: bool = False,
):
    # Extra arguments checks
    if columns % group_size != 0:
        raise ExitException(
            2,
            f"Column ({columns}) must be an integer multiple of group size ("
            f"{group_size})"
        )
    # Open input file as binary
    with open(infile, "rb") as infile:
        # Open output file as text, but don't close the underlying fd if
        # an already existing fd number is given.
        # This avoid closing stdout upon exiting the runtime context
        # if we
        # passed its fd number.
        with open(
                outfile, "wt", closefd=not isinstance(outfile, int)
        ) as outfile:

            if pastebin is not None:
                # Dumping to in-memory buffer
                outfile = io.StringIO()

            if not infile.seekable() and seek != 0:
                raise ExitException(
                    1,
                    "Input file couldn't be attached a seekable file descriptor"
                )
            if seek < 0:
                # We'll have to read the whole file first to compute its total
                # length in order to properly compute the offset
                total_length = len(infile.read())
                offset += total_length + seek
                infile.seek(seek, SEEK_END)
            else:
                infile.seek(seek, SEEK_SET)
                offset += seek
            n_read = 0
            while length is None or length - n_read > 0:
                # Check if we have bytes left before printing the line, if
                # infile is seekable().
                # This will avoid us printing offset if EOF was reached on
                # the last group of the previous line
                if not check_eof(infile):
                    break
                if length is None:
                    # We can't read more than columns byte per line anyway,
                    # this just means "no length limit"
                    length_cap = columns
                else:
                    length_cap = length - n_read
                n_read += write_dump_line(
                    infile, outfile, columns, group_size, n_read + offset,
                    length_cap, decimal, upper, little_endian
                )

            if pastebin:
                outfile.seek(0)
                print(
                    upload_paste(
                        outfile.read(),
                        f"dev202-{infile.name}-{datetime.now()}",
                        pastebin
                    )
                )


def entrypoint():
    try:
        main(**parse_args())
    except ExitException as e:
        e.panic()
    except Exception as e:
        print(f"{type(e).__name__}: {e}", file=sys.stderr)
        exit(1)
    exit(0)


if __name__ == '__main__':
    entrypoint()
