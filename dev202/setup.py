#!/usr/bin/env python
import os
from setuptools import setup

from dev202 import version

NAME = "dev202"

with open("requirements.txt", "rt") as requirements:
    setup(
        author="Morgane Austreelis",
        author_email="dev202@pypy.dev.austreelis.net",
        classifiers=[
            "Development Status :: 4 - Beta",
            "License :: Freely Distributable",
            "Environment :: Console",
            "Intended Audience :: End Users/Desktop",
            "Natural Language :: English",
            "Programming Language :: Python :: 3.9",
            "Topic :: Utilities",
        ],
        name=NAME,
        version=version,
        url=f"gitlab.com/austreelis/dev2-competencies/-/tree/main/{NAME}",
        install_requires=requirements.read(),
        packages=[NAME],
        package_dir={NAME: os.environ.get("SETUP_SRC_DIR", "")},
        entry_points={
            "console_scripts": [f"{NAME}={NAME}.__main__:entrypoint"]
        },
        zip_safe=True,
    )
