# If you don't know what this file is, you can safely ignore it
{
  inputs = {
    devshell.url = "github:numtide/devshell";
    nixpkgs.url = "nixpkgs";
  };

  outputs = { devshell, nixpkgs, self, ... } @ inputs:
  with builtins; with nixpkgs.lib;
  let
    systems = [ "x86_64-linux" ];
    mkOutputs = system: let

      pkgs = nixpkgs.lib.recursiveUpdate nixpkgs (import nixpkgs {
        inherit system;
        overlays = [ devshell.overlay ];
      });

      pyPkgs = pkgs.python39.pkgs;

      getVersion = src: concatStringsSep "."
        (filter isString
        (split ","
        (replaceStrings [ " " ] [ "" ]
        (flip elemAt 0
        (match "__version__ *= *\\(([0-9 ,]*)\\).*"
        (readFile "${src}/__init__.py")
      )))));

      # Change module layout from flat (all files in the devXXX dir) to a
      # standard layout.

      mkDevDrvBase = pname: rec {
        version = getVersion src;

        src = "${self}/${pname}";
        dontUnpack = true;

        SETUP_SRC_DIR = pname;

        configurePhase = ''
          cp -rL $src ${pname}
          cp ${pname}/setup.py setup.py
          cp ${pname}/requirements.txt requirements.txt
        '';

        meta = {
          homepage = "https://gitlab/austreelis/dev2-competencies/-/tree/main/${pname}";
          license = licenses.free;
        };
      };

      mkDevWheel = pname': deps: let
        base = mkDevDrvBase pname';
      in pkgs.stdenv.mkDerivation (base // rec {
        pname = "python3.9-${pname'}-wheel";

        buildInputs = [ (pkgs.python39.withPackages (ps: with ps;
          [ setuptools wheel ] ++ (attrValues deps)
        )) ];

        buildPhase = "python setup.py egg_info -Db '' sdist bdist_wheel";
        installPhase = "cp -T dist/${pname'}-${base.version}-py3-none-any.whl $out";
      });

      mkDevPkg = pname: deps: pyPkgs.buildPythonPackage (
       (mkDevDrvBase pname)
       // rec {
         inherit pname;
         doCheck = false;

         propagatedBuildInputs = attrValues deps;
         pythonImportsCheck = attrNames deps;
       }
     );

      devModules = with pyPkgs; {
        dev202 = { inherit requests; };
      };

    in {
      devShell.${system} = pkgs.devshell.fromTOML ./devshell.toml;

      packages.${system} =
        (mapAttrs mkDevPkg devModules)
        // (mapAttrs'
          (pname: deps: {
            name = "${pname}-wheel";
            value = mkDevWheel pname deps;
          })
          devModules
        )
      ;
    };
  in with builtins; foldl'
    nixpkgs.lib.recursiveUpdate
    { }
    (map mkOutputs systems);
}
